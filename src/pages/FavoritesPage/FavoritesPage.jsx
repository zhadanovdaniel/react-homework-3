
import Card from "../../components/Card/Card";
import React, { useState, useEffect } from "react";
export default function FavoritesPage ({products, setActiveStarsCount}){

  
    const [favoriteCards, setFavoriteCards] = useState([]);
  
    useEffect(() => {
      const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
      setFavoriteCards(favorites);
      
    }, [favoriteCards])

 
    return (
      <>  
          <h1>Твої лайкнуті футболки:</h1>
          {favoriteCards.length === 0 ? (
              <h2>Поки пусто...</h2>
          ) : (
              <div className="cardWrapper">
                  {products
                      .filter((item) => favoriteCards.includes(item.article))
                      .map((item, index) => (
                          <Card
                              key={index}
                              src={item.image}
                              alt={item.name}
                              productName={item.name}
                              productPrice={item.price}
                              productArticle={item.article}
                              productColor={item.color}
                              setActiveStarsCount={setActiveStarsCount}
                              ModalOpen={item.ModalOpen}
                              activeStar={true}
                          />
                      ))}
              </div>
          )}
      </>
  )
}