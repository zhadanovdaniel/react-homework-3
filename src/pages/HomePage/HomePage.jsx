
import HomeBody from "./HomeBody";
import React, { useState, useEffect } from "react";
import CardWrapper from "../../components/CardWrapper/CardWrapper";

//має бути три масиви для фейворіта для кошика і продактс як стейти
export default function HomePage(props) {
  
const {products, setIsAdded, setActiveStarsCount,isModalOpen,openModal,closeModal,activeArticle, setActiveArticle,handleCartClick} = props;
  return (
    <>
      <div className="HomePage">
      
        <HomeBody>
          <CardWrapper
            products={products}
            setIsAdded={setIsAdded}
            setActiveStarsCount={setActiveStarsCount}
            isModalOpen={isModalOpen}
            openModal={openModal}
            closeModal={closeModal}
            activeArticle={activeArticle}
            setActiveArticle={setActiveArticle}
            handleCartClick={handleCartClick}
          />
        </HomeBody>
      </div>
    </>
  );
}
